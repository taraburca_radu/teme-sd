#include<iostream>

struct NOD
{
	int key;
	NOD *st;
	NOD *dr;
	NOD *parent;
	NOD(int k)
	{
		key = k;
		st = dr = parent = nullptr;
	}

};
class ARBORE_CAUT
{
public:
	NOD *radacina;

	ARBORE_CAUT()
	{
		radacina = nullptr;
	}

	void Insert(NOD *z)
	{
		NOD *y = nullptr;
		NOD *x = radacina;

		while (x != nullptr)
		{
			y = x;
			if (z->key < x->key)
				x = x->st;
			else
				x = x->dr;
		}
		z->parent = y;
		if (y == nullptr)
			radacina = z;
		else
		{
			if (z->key < y->key)
				y->st = z;
			else
				y->dr = z;
		}
	}
	
	NOD *Maxim(NOD *x)
	{
		NOD*y = x;
		while (y->dr != nullptr)
		{
			y = y->dr;

		}
		return y;
	}
	
	NOD *Minim(NOD *x)
	{
		NOD*y = x;
		while (y->st!=nullptr)
		{
			y = y->st;

		}
		return y;
	}

	NOD *Succesor(NOD *y)
	{
		if (y->dr != nullptr)
			return Minim(y->dr);
		else
		{
			NOD *parinte = y->parent;
			while (parinte!=nullptr && y == parinte->dr)
			{
				y = parinte;
				parinte = parinte->parent;
			}
			return parinte;
		}
	}

	NOD *Predecesor(NOD *y)
	{
		if (y->st != nullptr)
			return Maxim(y->st);
		else
		{
			NOD *parinte = y->parent;
			while (parinte != nullptr && y == parinte->st)
			{
				y = parinte;
				parinte = parinte->parent;
			}
			return parinte;
		}
	}

	NOD *Search(int k)
	{
		NOD *x = radacina;
		while (x != nullptr && x->key != k)
			if (k < x->key)
				x = x->st;
			else
				x = x->dr;
		return x;
	}

	void Transplant(NOD *u, NOD *v)
	{
		if (u->parent == nullptr)
			radacina = v;
		else
			if (u = u->parent->st)

				u->parent->st = v;
			else
				u->parent->dr = v;
		if (v != nullptr)
			v->parent = u->parent;
				
	}
	
	void Delete(NOD *z)
	{
		if (z->st == nullptr)
			Transplant(z, z->dr);
		else
			if (z->dr == nullptr)
				Transplant(z, z->st);
			else
			{
				NOD *y = Succesor(z);
				if (y != z->dr);
					{
					Transplant(y, y->dr);
					y->dr = z->dr;
					z->dr->parent = y;
					}


					Transplant(z, y);
					y->st = z->st;
					z->st->parent = y;
			}
	}

	void DELETE(NOD *z)
	{
		NOD* y = Search(z->key);
		Delete(y);
	}

	void Postorder( NOD* node)
	{
		if (node == nullptr)
			std::cout << "Arborele este gol";

		Postorder(node->st);

		Postorder(node->dr);

		std::cout << node->key << " ";
	}

	void Inorder( NOD* node)
	{
		if (node == NULL)
			std::cout << "Arborele este gol";

		Inorder(node->st);

		std::cout << node->key << " ";

		Inorder(node->dr);
	}

	void Preorder( NOD* node)
	{
		if (node == nullptr)
			std::cout << "Arborele este gol";
		
		std::cout << node->key << " ";

		Preorder(node->st);

		Preorder(node->dr);
	}

	void Print_Tree()
	{
		int x;
		std::cout << "Alegeti o modalitate de afisare: \n";
		std::cout << "1)Afisare in preordine\n";
		std::cout << "2)Afisare in inordine\n";
		std::cout << "3)Afisare in postordine\n";
		
		std::cin >> x;
		switch (x)
		{
		case 1:
		{
			Preorder(radacina);
			break;
		}
		case 2:
		{
			Inorder(radacina);
			break;
		}
		case 3:
		{
			Postorder(radacina);
			break;
		}
		case 4:
		{
			break;
		}
		default:
			break;
		}
	}

	void construct(int *v,int n)
	{
		NOD *a;
		for (int i = 0; i < n; i++)
		{
			a = new NOD(v[i]);
			Insert(a);
		}
		
 	}
	
	void Emptry()
	{
		delete[] radacina;
	}
	
};

int main()
{
	ARBORE_CAUT b;
	int *v;
	int n,x;
	

	std::cout << "1)INSERT\n";
	std::cout << "2)Maxim\n";
	std::cout << "3)Minim\n";
	std::cout << "4)Succesor\n";
	std::cout << "5)Predecesor\n";
	std::cout << "6)Cauta un nod cu valoarea k\n";
	std::cout << "7)Sterge un nod cu valoarea k\n";
	std::cout << "8)Selecteaza modul de afisare dorit si afiseaza arborele \n";
	std::cout << "9)Creeaza un arbore in functie de un vector de chei\n";
	std::cout << "10)Goleste arborele\n";
	std::cout << "11)EXIT\n";
	std::cin >> x;
	while (x != 11)
	{
		switch (x)
		{
		case 1:
		{	int k;
		std::cin >> k;
		NOD*c = new NOD(k);
			b.Insert(c);
			break;
		}
		case 2:
		{
			b.Maxim(b.radacina);
			break;
		}
		case 3:
		{
			b.Minim(b.radacina);

			break;
		}
		case 4:
		{	int k;
		std::cin >> k;
		NOD*c = new NOD(k);
			b.Succesor(c);
			break;
		}
		case 5:
		{
			int k;
			std::cin >> k;
			NOD*c = new NOD(k);
			b.Predecesor(c);
			break;
		}
		case 6:
		{
			int k;
			std::cin >> k;
			b.Search(k);
			break;
		}
		case 7:
		{
			int k;
			std::cin >> k;
			NOD*c = new NOD(k);
			b.DELETE(c);
			break;
		}
		case 8:
		{
			b.Print_Tree();

			break;
		}
		case 9:
		{
			std::cin >> n;
			v = new int[n];
			for (int i = 0; i < n; i++)
				std::cin >> v[i];
			b.construct(v, n);
			break;
		}
		case 10:
		{
			b.Emptry();

			break;
		}

		default:
			break;
		}
	}
	system("pause");
}