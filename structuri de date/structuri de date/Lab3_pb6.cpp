#include< iostream>
#include< fstream>
std::ifstream f("Text3.txt");

struct NOD
{
	int info;
	NOD *STANGA, *DREAPTA;
	

};
struct Arbore
{
	NOD *root;
	int *SRD, *RSD;
	Arbore()
	{
		root = NULL;
	}

	void citire(int n)
	{
		SRD = new int[n];
		RSD = new int[n];
		for (int i = 0; i < n; i++)
			f >> SRD[i];
		for (int i = 0; i < n; i++)
			f >> RSD[i];
	}	

	NOD* newnod(int x)
	{
		NOD* nod = new NOD;
		nod->info = x;
		nod->STANGA = NULL;
		nod->DREAPTA = NULL;

		return nod;
	}

	int search(int strt, int end, int value)
	{

		for (int index = strt; index <= end; index++) {
			if (SRD[index] == value)
				return index;
		}
	}
	NOD* reconstructie(int inStart, int inEnd)
	{
		static int preIndex = 0;

		if (inStart > inEnd)
			return NULL;
		NOD* treenode = newnod(RSD[preIndex++]);
		if (inStart == inEnd)
			return treenode;
		int inIndex = search(inStart, inEnd, treenode->info);
		treenode->STANGA = reconstructie(inStart, inIndex - 1);
		treenode->DREAPTA = reconstructie(inIndex + 1, inEnd);
		return treenode;

	}

	void display(NOD*root)
	{
		if (root == NULL)
			return;
		display(root->STANGA);
		display(root->DREAPTA);
		std::cout << root->info << " ";
	}
};





int main()
{
	Arbore t;
	int n, *SRD, *RSD;
	f >> n;
	t.citire(n);
	t.root = t.reconstructie(0, n - 1);
	t.display(t.root);
	system("pause");
}