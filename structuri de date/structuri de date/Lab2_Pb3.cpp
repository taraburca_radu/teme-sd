#include <iostream>

struct  coada 
{
	int *data;
	int size_max;
	int begin=0, end=-1;

	void push(int element)
	{
		data[++end] = element;
		
	}

	void pop()
	{
		for (int index = begin; index < end; index++)
			data[index] = data[index + 1];
		end--;
	}

	void isEmpty()
	{
		if (end == -1)
			std::cout << "It is empty" << std::endl;
	}

	int Front()
	{
		return data[begin];
	}

};

int main()
{
	coada obj;
	int n,x;
	std::cin >> n;
	obj.size_max = n;
	obj.data = new int[obj.size_max];
	for (int i = 0; i < n; i++)
	{
		std::cin >> x;
		obj.push(x);

	}
	for (int i = 0; i < n; i++)
	{
		std::cout << obj.Front() << " ";
		obj.pop();
	}
	system("pause");
	return 0;
}