#include<iostream>
#include<queue>


//0-negru
//1-rosu

struct nod
{
	int info;
	bool culoare;
	nod* parinte;
	nod* stanga;
	nod* dreapta;

	nod(int valoare = 0)
	{
		info = valoare;
		culoare = 1;
		parinte = stanga = dreapta = NULL;
	}

};

struct ARN
{
	nod *radacina = new nod;
	nod* nill = new nod;


	ARN()
	{
		nill->info = NULL;
		nill->culoare = 0;
		nill->stanga = nill->dreapta = nill->parinte = NULL;

	}

	void rotireStanga(nod* x)
	{
		nod* y = x->dreapta;
		x->dreapta = y->stanga;
		if (y->stanga != nill)
			y->stanga->parinte = x;
		y->parinte = x->parinte;
		if (x->parinte == nill)
			radacina = y;
		else
		{
			if (x == x->parinte->stanga)
				x->parinte->dreapta = y;
			else
				x->parinte->dreapta = y;
		}
		y->stanga = x;
		x->parinte = y;
	}

	void rotireDreapta(nod* x)

	{
		nod*y = x->stanga;
		x->stanga = y->dreapta;
		if (y->dreapta != nill)
			y->dreapta->parinte = x;
		y->parinte = x->parinte;
		if (x->parinte == nill)
			radacina = y;
		else
		{
			if (x == x->parinte->dreapta)
				x->parinte->dreapta = y;
			else x->parinte->stanga = y;
		}

		y->dreapta = x;
		x->parinte = y;

	}


	void insertRepara(nod* z)
	{
		nod* u = new nod(NULL);
		while (z->parinte->culoare == 1)
		{
			if (z->parinte == z->parinte->parinte->stanga)
				u = z->parinte->parinte->dreapta;
			if (u->culoare == 1)
			{
				z->parinte->culoare = 0;
				u->culoare = 0;
				z->parinte->parinte->culoare = 1;
				z = z->parinte->parinte;
			}

			else
			{
				if (z == z->parinte->dreapta)
				{
					z = z->parinte;
					rotireStanga(z);
				}

				z->parinte->culoare = 0;
				z->parinte->parinte->culoare = 1;
				rotireDreapta(z->parinte->parinte);

			}


		}
		radacina->culoare = 0;

	}


	nod* cautare_binara(int cheie)
	{
		nod *x = radacina;
		while (x != NULL && x->info != cheie)
			if (cheie < x->info)
				x = x->stanga;
			else x = x->dreapta;

			if (x == NULL)
				return NULL;
			else return x;
	}


	nod minim(int cheie)
	{
		nod* radacina = cautare_binara(cheie);
		if (radacina == NULL)
			return NULL;
		nod*y = radacina;
		while (y->stanga != NULL)
			y = y->stanga;
		return *y;
	}

	nod maxim(int cheie)
	{
		nod* radacina = cautare_binara(cheie);
		nod* y = radacina;
		while (y->dreapta != NULL)
			y = y->dreapta;
		return *y;
	}

	nod succesor(int cheie)
	{
		nod* succesorPt = cautare_binara(cheie);
		nod* y(NULL);
		if (succesorPt->dreapta != NULL)
		{
			y = &minim(succesorPt->dreapta->info);
			return *y;
		}
		y = succesorPt->parinte;
		while (y != NULL && succesorPt == y->dreapta)
		{
			succesorPt = y;
			y = y->dreapta;
		}

		return *y;

	}
	nod predecesor(int cheie)
	{
		nod* predecesorPt = cautare_binara(cheie);
		nod* y(NULL);
		if (predecesorPt->stanga != NULL)
		{
			y = &maxim(cheie);
			return *y;
		}
		y = predecesorPt->parinte;
		while (y != NULL && predecesorPt == y->stanga)
		{
			predecesorPt = y;
			y = y->stanga;
		}

		return *y;

	}



	void insert(int valoare)
	{
		nod *z = new nod(valoare);
		nod *y(NULL);
		nod *x = radacina;
		while (x != NULL)
		{
			y = x;
			if (z->info < x->info)
				x = x->stanga;
			else x = x->dreapta;
		}
		z->parinte = y;
		if (y == NULL)
			radacina = z;
		else {
			if (z->info < y->info)
				y->stanga = z;
			else y->dreapta = z;
		}
		insertRepara(radacina);
	}

	void construct()
	{
		int nrChei, *vecChei;
		std::cout << "Cate noduri inserati? ";
		std::cin >> nrChei;
		vecChei = new int[nrChei];
		std::cout << std::endl << "Introduceti " << nrChei << " noduri: ";
		for (unsigned index = 0; index < nrChei; ++index)
			std::cin >> vecChei[index];


		for (unsigned index = 0; index < nrChei; ++index)
			insert(vecChei[index]);


	}



	void transplant(nod*u, nod* v)
	{
		if (u->parinte == nill)
			radacina = v;
		else
		{
			if (u == u->parinte->stanga)
				u->parinte->stanga = v;
			else
				u->parinte->dreapta = v;
		}
		v->parinte = u->parinte;
	}


	void stergere(nod *z)
	{
		//culoare = z->culoare;
		if (z->stanga == nill)
		{
			x = z->dreapta;
			transplant(z, x);
		}
		else
		{
			if (z->dreapta == nill)
			{
				x = z.stanga;
				transplant(z, x);

			}
			else
			{
				y = succesor(z->info);
				culoare = y->culoare
					x = y->dreapta;
				if (y.parinte == z)
					x.parinte = y;
				else
				{
					transplant(y, x);
					y->dreapta = x->dreapta;
					z->dreapta->parinte = y;
				}
				transplant(z, y);
				y.stanga = z->stanga;
				y.stanga.parinte = y;
				y.culoare = z->culoare;
			}

		}
		if (culoare == 0)
			StergereRepara(x);

	}
	void parcurgerePostordine(nod* radacina)

	{
		if (radacina == NULL)
			return;
		else
		{
			if (radacina != NULL)
			{
				parcurgerePostordine(radacina->stanga);
				parcurgerePostordine(radacina->dreapta);
				std::cout << radacina->info << " ";
			}
		}

	}

	void parcurgereInordine(nod* radacina)
	{
		if (radacina == NULL)
			return;

		if (radacina != NULL)
		{

			parcurgereInordine(radacina->stanga);
			std::cout << radacina->info << " ";
			parcurgereInordine(radacina->dreapta);
		}

	}

	void parcurgerePreordine(nod* radacina)
	{
		if (radacina == NULL)
			return;

		if (radacina != NULL)
		{
			std::cout << radacina->info << " ";
			parcurgerePreordine(radacina->stanga);
			parcurgerePreordine(radacina->dreapta);

		}
	}
	void parcurgereNivele(nod *radacina)
	{
		std::queue <nod*> coada;

		if (radacina == NULL)
			return;


		coada.push(radacina);

		while (!coada.empty())
		{
			nod* aux = coada.front();
			std::cout << coada.front()->info << " ";
			coada.pop();

			if (aux->stanga != NULL)
				coada.push(aux->stanga);
			if (aux->dreapta != NULL)
				coada.push(aux->dreapta);
		}
	}

	void deleteArbore(nod* radacina)
	{

		if (radacina == NULL)
			return;
		deleteArbore(radacina->stanga);
		deleteArbore(radacina->dreapta);

		free(radacina);
		radacina = NULL;

	}


};

int main()
{
	ARN arb;
	int valoare;
	int optiune;

	std::cout << "-1";
	std::cout << std::endl;
	//std::cout<<arb.succesor()

	do {
		std::cout << "-------------------------------------------------------------\n";
		std::cout << "Ce doriti sa efectuati? \n";
		std::cout << "(1) Construire arbore pornind de la un vector.\n";
		std::cout << "(2) Insereaza un nou element.\n";
		std::cout << "(3) Afiseaza elementul cu valoare maxima.\n";
		std::cout << "(4) Afisarea elementului cu valoare minima \n";
		std::cout << "(5) Afisarea succesorului unui nod. \n";
		std::cout << "(6) Afisarea predecesorului unui nod. \n";
		std::cout << "(7) Cauta un element in arbore \n";
		std::cout << "(8) Afisare elemente in inordine. \n";
		std::cout << "(9) Afisare elemente in postordine. \n";
		std::cout << "(10) Afisare elemente in preordine. \n";
		std::cout << "(11) Afisare elemente in ordinea de pe nivele. \n";
		std::cout << "(12) Sterge un element. \n";
		std::cout << "(13) Sterge completa arbore. \n";

		std::cout << "(14) Iesire aplicatie. \n";
		std::cout << "--------------------------------------------------------------\n";
		std::cout << std::endl << std::endl;
		std::cout << "Introduceti optiunea ";
		std::cin >> optiune;


		switch (optiune)
		{

		case 1: arb.construct();
			break;
		case 2:
			std::cout << "Ce valoare inserati? ";
			std::cin >> valoare;
			arb.insert(valoare);
			std::cout << "Arbore construit " << std::endl;
			break;
		case 3:
			std::cout << "Incepand cu ce radacina cautati elementul?";
			std::cin >> valoare;
			std::cout << arb.maxim(valoare).info;
			break;
		case 4:
			std::cout << "Incepand cu ce radacina cautati elementul?";
			std::cin >> valoare;
			std::cout << arb.minim(valoare).info;
			break;
		case 5:
			std::cout << "Pentru care nod se cauta succesorul? ";
			std::cin >> valoare;
			std::cout << "Succesorul este: " << arb.succesor(valoare).info;

		case 6:
			std::cout << "Pentru care nod se cauta predecesor?";
			std::cin >> valoare;
			std::cout << arb.predecesor(valoare).info;
			break;
		case 7:
			std::cout << "Ce element cautati?";
			std::cin >> valoare;
			if (arb.cautare_binara(valoare) != NULL)
				std::cout << "Element existent";
			else std::cout << "Element inexistent";
			break;
		case 8:
			arb.parcurgereInordine(arb.radacina);
			break;
		case 9:
			arb.parcurgerePostordine(arb.radacina);
			break;
		case 10:
			arb.parcurgerePreordine(arb.radacina);
			break;
		case 11:
			arb.parcurgereNivele(arb.radacina);
			break;
		case 12:
			std::cout << "Ce valoare stergeti? ";
			std::cin >> valoare;
			arb.stergere(valoare);
			break;
		case 13:
			arb.deleteArbore(arb.radacina);
			std::cout << "Arbore sters complet ";
			break;
		case 14: exit(1);
		defaul: "Optiune incorecta \n";
		}


	} while (optiune != 14);



}