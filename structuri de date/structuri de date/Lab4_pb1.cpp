#include<iostream>

class priority_queue
{
	int *DATA;
	int SIZE;
	int CAPACITY;
public:
	priority_queue(int dim = 50)
	{
		
		CAPACITY = dim;
		DATA = new int[dim];
		SIZE = 0;
	};

	void INSERT(int x)
	{
		DATA[SIZE] = 0;
		SIZE = SIZE + 1;
		INCREASE_KEY(SIZE - 1, x);
	}

	int EXTRACT_MAX()
	{
		int max;
		if (SIZE < 1)
			exit;
		else
		{
			max = DATA[0];
			DATA[0] = DATA[SIZE - 1];
			SIZE = SIZE - 1;
			MAX_HEAPFY(0);

		}
		return max;
	}

	int MAX_ELEMENT()
	{
		return DATA[0];
	}

	void INCREASE_KEY(int i, int val)
	{
		int p;
		if (val < DATA[i])
			std::cout << "valoarea este prea mica";
		else
		{
			DATA[i] = val;
			p = (i - 1) / 2;
			while (i > 0 && DATA[p] < val)
			{
				DATA[i] = DATA[p];
				i = p;
				p = (i - 1) / 2;
			}
			DATA[i] = val;
		}

	}

	void MAX_HEAPFY(int i)
	{
		int st, dr, imax;
		st = 2 * i;
		dr = 2 * i + 1;
		imax = i;
		if (st<SIZE && DATA[st]>DATA[i])
			imax = st;
		if (dr<SIZE && DATA[dr]>DATA[i])
			imax = dr;
		if (imax != i)
		{
			int aux = DATA[imax];
			DATA[imax] = DATA[i];
			MAX_HEAPFY(imax);
		}
	}

	void CONSTRUCT_HEAP()
	{
		for (int i = SIZE / 2; i >= 0; i--)
			MAX_HEAPFY(i);
	
	}

	

};



int main()
{
	priority_queue q;
	q.INSERT(23);
	q.INSERT(15);
	q.INSERT(24);
	q.INSERT(6);
	q.INSERT(1);

	

	int x,n;
	std::cout << "1)INSERT\n";
	std::cout << "2)CONSTRUCT HEAP\n";
	std::cout << "3)MAX ELEMENT\n";
	std::cout << "9)EXIT\n";
	std::cin >> x;
	while (x != 9)
	{
		switch (x)
		{
		case 1:
		{
			std::cin >> n;
			q.INSERT(n);
			break;
		}
		case 2:
		{
			q.CONSTRUCT_HEAP();
			break;
		}
		case 3:
		{
			std::cout << q.EXTRACT_MAX();

			break;
		}
		case 4:
		{
			std::cout << q.MAX_ELEMENT();

			break;
		}

		default:
			break;
		}
	}
	system("pause");
}