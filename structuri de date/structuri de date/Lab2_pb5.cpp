#include <iostream>

struct nod
{
	int info;
	nod *urmatorul;
};
struct lista
{
	nod *cap;
	lista()
	{
		cap = NULL;
	}

	void insert(int k)
	{
		nod* p = new nod;
		p->info = k;
		p->urmatorul = cap;
		cap = p;
	}
	void print_list()
	{
		nod *hub=cap ;
		while (hub != NULL)
		{
			std::cout << hub->info << " ";
			hub = hub->urmatorul;
		}
	}
	nod* search(int x)
	{
		int i = 0;
		while (cap != NULL && i < x)
		{
			cap = cap->urmatorul;
			i++;
		}
		if (i == x)
			return cap;
		else
			return NULL;
	}
	void Delete(nod* predecesor)
	{
		nod *targget = predecesor->urmatorul;
		predecesor->urmatorul = predecesor->urmatorul->urmatorul;
		delete targget;
	}
	void DELETE(int poz)
	{
		if (poz == 0)
		{
			nod* targget = cap;
			cap = cap->urmatorul;
			delete targget;
		}
		else
		{
			nod* predecesor = search(poz - 1);
			Delete(predecesor);
		}
	}
	bool isEmpty()
	{
		if (cap == NULL)
			return true;
		return false;
	}
	void insert_after(int k)
	{
		nod *elem_final = new nod;
		elem_final->info = k;
		elem_final->urmatorul = NULL;

		if (cap == NULL)
			cap = elem_final;
		else
		{
			nod *nod_curent = cap;
			while (nod_curent->urmatorul != NULL)
				nod_curent = nod_curent->urmatorul;

			nod_curent->urmatorul = elem_final;
		}
	}
	void inverse()
	{
		
		nod *curent = cap;
		nod *anteriorul = NULL, *urm = NULL;


		while (curent != NULL)
		{
			urm = curent->urmatorul;

			curent->urmatorul = anteriorul;

			anteriorul = curent;
			curent = urm;
		}
		cap = anteriorul;
	}
};

int main()
{
	lista *p;
	int n;
	//std::cin >> n;
	p = new lista;
	p->insert(3);
	p->insert(4);
	p->insert(1);
	p->insert_after(98);
	p->print_list();
	int x;
	std::cout << "Alegeti o optiune!\n";
	std::cout << "0)Stop\n";
	std::cout << "1)Inserarea  in lista a unui nr n\n";
	std::cout << "2)Stergerea nodului n\n";
	std::cout << "3)Afisarea listei\n";
	std::cout << "4)Cautarea unui nod n din lista\n";
	std::cout << "5)Verificare daca lista e goala\n";
	std::cout << "6)Inserare dupa capatul listei a nr n\n";
	std::cout << "7)Inversarea unei liste\n";

	do
	{


		std::cin >> x;

		switch (x)
		{
		case 1:
		{	std::cout << "citeste o valoare pt n\n";
		std::cin >> n;
		p->insert(n);

		break;
		}
		case 2:
		{std::cout << "citeste o valoare pt n\n";
		std::cin >> n;
		p->DELETE(n);
		break;

		}
		case 3:
		{
			p->print_list();
			break;
		}
		case 4:
		{std::cout << "citeste o valoare pt n\n";
		std::cin >> n;
		std::cout << p->search(n);
		break;
		}
		case 5:
		{
			if (p->isEmpty() == true)
				std::cout << "lista este goala\n";
			else
				std::cout << "lista nu este goala\n";
			break;
		}
		case 6:
		{std::cout << "citeste o valoare pt n\n";
		std::cin >> n;
		p->insert_after(n);
		break;
		}
		case 7:
		{
			p->inverse();
			break;
		}
		default:

			break;
		}
		std::cout << "Alegeti o alta obtiune\n";
	} while (x != 0);
	system("pause");
}