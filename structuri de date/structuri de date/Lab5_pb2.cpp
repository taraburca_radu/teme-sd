#include<iostream>
template<class T>
struct NOD
{
	T key;
	int fb;
	NOD *st;
	NOD *dr;
	NOD *parent;
	NOD(T k)
	{
		key = k;
		fb = 0;
		st = dr = parent = nullptr;
	}

};
class AVL
{
public:
	NOD<int> *radacina;

	AVL()
	{
		radacina = nullptr;
	}
	void Insert(NOD<int> *z)
	{
		NOD<int> *y = nullptr;
		NOD<int> *x = radacina;

		while (x != nullptr)
		{
			y = x;
			if (z->key < x->key)
				x = x->st;
			else
				x = x->dr;
		}
		z->parent = y;
		if (y == nullptr)
			radacina = z;
		else
		{
			if (z->key < y->key)
				y->st = z;
			else
				y->dr = z;
		}
		Insert_repara(radacina);

	}
	
	void Insert_repara(NOD<int> *z)
	{

	}

	NOD<int> *Maxim(NOD<int> *x)
	{
		NOD<int> *y = x;
		while (y->dr != nullptr)
		{
			y = y->dr;

		}
		return y;
	}

	NOD<int> *Minim(NOD<int> *x)
	{
		NOD<int> *y = x;
		while (y->st != nullptr)
		{
			y = y->st;

		}
		return y;
	}

	NOD<int> *Succesor(NOD<int> *y)
	{
		if (y->dr != nullptr)
			return Minim(y->dr);
		else
		{
			NOD<int> *parinte = y->parent;
			while (parinte != nullptr && y == parinte->dr)
			{
				y = parinte;
				parinte = parinte->parent;
			}
			return parinte;
		}
	}

	NOD<int> *Predecesor(NOD<int> *y)
	{
		if (y->st != nullptr)
			return Maxim(y->st);
		else
		{
			NOD<int> *parinte = y->parent;
			while (parinte != nullptr && y == parinte->st)
			{
				y = parinte;
				parinte = parinte->parent;
			}
			return parinte;
		}
	}
	
	NOD<int> *Search(int k)
	{
		NOD<int> *x = radacina;
		while (x != nullptr && x->key != k)
			if (k < x->key)
				x = x->st;
			else
				x = x->dr;
		return x;
	}

	void Rot_st(NOD<int> *x)
	{
		NOD<int> *y = x->dr;
		x->dr = y->st;
		if (y->st != nullptr)
			y->st->parent = x;
		y->parent = x->parent;
		if (x->parent == nullptr)
			radacina = y;
		else
			if (x == x->parent->st)
				y = x->parent->st;
			else
				y = x->parent->dr;
		x->parent = y;
		y->st = x;
	}

	void Rot_dr(NOD<int> *x)
	{
		NOD<int> *y = x->st;
		x->st = y->dr;
		if (y->dr != nullptr)
			y->dr->parent = x;
		y->parent = x->parent;
		if (x->parent == nullptr)
			radacina = y;
		else
			if (x == x->parent->dr)
				y = x->parent->dr;
			else
				y = x->parent->st;
		x->parent = y;
		y->dr = x;
	}

	void DELETE(NOD<int> *x)
	{

	}

	void DELETE_REPARA(NOD<int> *x)
	{

	}





	void Postorder(NOD<int>* node)
	{
		if (node == nullptr)
			std::cout << "Arborele este gol";

		Postorder(node->st);

		Postorder(node->dr);

		std::cout << node->key << " ";
	}

	void Inorder(NOD<int>* node)
	{
		if (node == nullptr)
			std::cout << "Arborele este gol";

		Inorder(node->st);

		std::cout << node->key << " ";

		Inorder(node->dr);
	}

	void Preorder(NOD<int> * node)
	{
		if (node == nullptr)
			std::cout << "Arborele este gol";

		std::cout << node->key << " ";

		Preorder(node->st);

		Preorder(node->dr);
	}

	void Print_Tree()
	{
		int x;
		std::cout << "Alegeti o modalitate de afisare: \n";
		std::cout << "1)Afisare in preordine\n";
		std::cout << "2)Afisare in inordine\n";
		std::cout << "3)Afisare in postordine\n";

		std::cin >> x;
		switch (x)
		{
		case 1:
		{
			Preorder(radacina);
			break;
		}
		case 2:
		{
			Inorder(radacina);
			break;
		}
		case 3:
		{
			Postorder(radacina);
			break;
		}
		case 4:
		{
			break;
		}
		default:
			break;
		}
	}


	void Emptry()
	{
		delete[] radacina;
	}


	void construct(int *v, int n)
	{
		NOD<int> *a;
		for (int i = 0; i < n; i++)
		{
			a = new NOD<int>(v[i]);
			Insert(a);
		}
	}


	void Merge(AVL T1, AVL T2)
	{
		if ( T1.Maxim(T1.radacina) < T2.Minim(T2.radacina) || T1.Minim(T1.radacina) > T2.Maxim(T2.radacina))
			

	}

};
	


int main()
{

	AVL b;
	int *v;
	int n, x;


	std::cout << "1)INSERT\n";
	std::cout << "2)Maxim\n";
	std::cout << "3)Minim\n";
	std::cout << "4)Succesor\n";
	std::cout << "5)Predecesor\n";
	std::cout << "6)Cauta un nod cu valoarea k\n";
	std::cout << "7)Sterge un nod cu valoarea k\n";
	std::cout << "8)Selecteaza modul de afisare dorit si afiseaza arborele \n";
	std::cout << "9)Creeaza un arbore in functie de un vector de chei\n";
	std::cout << "10)Goleste arborele\n";
	std::cout << "11)Merge two AVl tree\n";
	std::cout << "12)EXIT\n";
	std::cin >> x;
	while (x != 12)
	{
		switch (x)
		{
		case 1:
		{	int k;
		std::cin >> k;
		NOD<int>*c = new NOD<int>(k);
		b.Insert(c);
		break;
		}
		case 2:
		{
			b.Maxim(b.radacina);
			break;
		}
		case 3:
		{
			b.Minim(b.radacina);

			break;
		}
		case 4:
		{	int k;
		std::cin >> k;
		NOD<int>*c = new NOD<int>(k);
		b.Succesor(c);
		break;
		}
		case 5:
		{
			int k;
			std::cin >> k;
			NOD<int>*c = new NOD<int>(k);
			b.Predecesor(c);
			break;
		}
		case 6:
		{
			int k;
			std::cin >> k;
			b.Search(k);
			break;
		}
		case 7:
		{
			int k;
			std::cin >> k;
			NOD<int>*c = new NOD<int>(k);
			b.DELETE(c);
			break;
		}
		case 8:
		{
			b.Print_Tree();

			break;
		}
		case 9:
		{
			std::cin >> n;
			v = new int[n];
			for (int i = 0; i < n; i++)
				std::cin >> v[i];
			b.construct(v, n);
			break;
		}
		case 10:
		{
			b.Emptry();

			break;
		}
		case 11:
		{
			AVL q,tr;
			std::cin >> n;
			v = new int[n];
			for (int i = 0; i < n; i++)
				std::cin >> v[i];
			q.construct(v, n);

			tr.Merge(q, b);

			break;
		}
		default:
			break;
		}
	}
	
	system("pause");
}