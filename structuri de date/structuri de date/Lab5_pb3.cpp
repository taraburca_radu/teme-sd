#include<iostream>
#define const R =true;
#define const N= false;
#define const R=1;
#define const N=0;

template<class T>
struct NOD
{
	T key;
	bool culoare;
	NOD *st;
	NOD *dr;
	NOD *parent;
	NOD(T k)
	{
		key = k;
		fb = 0;
		st = dr = parent = nullptr;
		culoare = R;
	}
	
	
};

class ARN
{
public:
	NOD<int> *Radacina;
	NOD<int> *NIL;
	ARN()
	{
		NIL->key = NULL;
		NIL->culoare = N;
		NIL->st = NIL->dr = NIL->parent = NULL;
	
	}

	void Insert(NOD<int> *z)
	{
		NOD<int> *y = nullptr;
		NOD<int> *x = Radacina;

		while (x != nullptr)
		{
			y = x;
			if (z->key < x->key)
				x = x->st;
			else
				x = x->dr;
		}
		z->parent = y;
		if (y == nullptr)
			Radacina = z;
		else
		{
			if (z->key < y->key)
				y->st = z;
			else
				y->dr = z;
		}

		Insert_repara(Radacina);
	}

	void Insert_repara(NOD<int> *z)
	{

	}


	
	void Rot_st(NOD<int> *x)
	{
		NOD<int> *y = x->dr;
		x->dr = y->st;
		if (y->st != NIL)
			y->st->parent = x;
		y->parent = x->parent;
		if (x->parent == NIL)
			Radacina = y;
		else
		{
			if (x == x->parent->st)
				y = x->parent->st;
			else
				y = x->parent->dr;
		}
		x->parent = y;
		y->st = x;
	}

	void Rot_dr(NOD<int> *x)
	{
		NOD<int> *y = x->st;
		x->st = y->dr;
		if (y->dr != NIL)
			y->dr->parent = x;
		y->parent = x->parent;
		if (x->parent == NIL)
			Radacina = y;
		else
			if (x == x->parent->dr)
				y = x->parent->dr;
			else
				y = x->parent->st;
		x->parent = y;
		y->dr = x;
	}



	NOD<int> *Search(int k)
	{
		NOD<int> *x = Radacina;
		while (x != nullptr && x->key != k)
			if (k < x->key)
				x = x->st;
			else
				x = x->dr;
		return x;
	}

	NOD<int> *Maxim(NOD<int> *x)
	{
		NOD<int> *y = x;
		while (y->dr != nullptr)
		{
			y = y->dr;

		}
		return y;
	}

	NOD<int> *Minim(NOD<int> *x)
	{
		NOD<int> *y = x;
		while (y->st != nullptr)
		{
			y = y->st;

		}
		return y;
	}

	NOD<int> *Succesor(NOD<int> *y)
	{
		if (y->dr != nullptr)
			return Minim(y->dr);
		else
		{
			NOD<int> *parinte = y->parent;
			while (parinte != nullptr && y == parinte->dr)
			{
				y = parinte;
				parinte = parinte->parent;
			}
			return parinte;
		}
	}

	NOD<int> *Predecesor(NOD<int> *y)
	{
		if (y->st != nullptr)
			return Maxim(y->st);
		else
		{
			NOD<int> *parinte = y->parent;
			while (parinte != nullptr && y == parinte->st)
			{
				y = parinte;
				parinte = parinte->parent;
			}
			return parinte;
		}
	}







	
	



	void Transplant(NOD<int>*u, NOD<int>* v)
	{
		if (u->parent == NIL)
			Radacina = v;
		else
		{
			if (u == u->parent->st)
				u->parent->st = v;
			else
				u->parent->dr = v;
		}
		v->parent = u->parent;
	}


	void DELETE(NOD<int> *x)
	{

	}

	void DELETE_REPARA(NOD<int> *x)
	{

	}
	
	



	





	void Postorder(NOD<int>* node)
	{
		if (node == nullptr)
			std::cout << "Arborele este gol";

		Postorder(node->st);

		Postorder(node->dr);

		std::cout << node->key << " ";
	}

	void Inorder(NOD<int>* node)
	{
		if (node == nullptr)
			std::cout << "Arborele este gol";

		Inorder(node->st);

		std::cout << node->key << " ";

		Inorder(node->dr);
	}

	void Preorder(NOD<int> * node)
	{
		if (node == nullptr)
			std::cout << "Arborele este gol";

		std::cout << node->key << " ";

		Preorder(node->st);

		Preorder(node->dr);
	}

	void Print_Tree()
	{
		int x;
		std::cout << "Alegeti o modalitate de afisare: \n";
		std::cout << "1)Afisare in preordine\n";
		std::cout << "2)Afisare in inordine\n";
		std::cout << "3)Afisare in postordine\n";

		std::cin >> x;
		switch (x)
		{
		case 1:
		{
			Preorder(Radacina);
			break;
		}
		case 2:
		{
			Inorder(Radacina);
			break;
		}
		case 3:
		{
			Postorder(Radacina);
			break;
		}
		case 4:
		{
			break;
		}
		default:
			break;
		}
	}


	void Emptry()
	{
		if (Radacina == NULL)
			return;
		delete[] Radacina;
	}


	void construct(int *v, int n)
	{
		NOD<int> *a;
		for (int i = 0; i < n; i++)
		{
			a = new NOD<int>(v[i]);
			Insert(a);
		}
	}


};



int main()
{
	ARN b;
	int *v;
	int n, x;

	std::cout << "1)INSERT\n";
	std::cout << "2)Maxim\n";
	std::cout << "3)Minim\n";
	std::cout << "4)Succesor\n";
	std::cout << "5)Predecesor\n";
	std::cout << "6)Cauta un nod cu valoarea k\n";
	std::cout << "7)Sterge un nod cu valoarea k\n";
	std::cout << "8)Selecteaza modul de afisare dorit si afiseaza arborele \n";
	std::cout << "9)Creeaza un arbore in functie de un vector de chei\n";
	std::cout << "10)Goleste arborele\n";
	std::cout << "11)EXIT\n";
	std::cin >> x;
	while (x != 11)
	{
		switch (x)
		{
		case 1:
		{	int k;
		std::cin >> k;
		NOD<int>*c = new NOD<int>(k);
		b.Insert(c);
		break;
		}
		case 2:
		{
			b.Maxim(b.Radacina);
			break;
		}
		case 3:
		{
			b.Minim(b.Radacina);

			break;
		}
		case 4:
		{	int k;
		std::cin >> k;
		NOD<int>*c = new NOD<int>(k);
		b.Succesor(c);
		break;
		}
		case 5:
		{
			int k;
			std::cin >> k;
			NOD<int>*c = new NOD<int>(k);
			b.Predecesor(c);
			break;
		}
		case 6:
		{
			int k;
			std::cin >> k;
			b.Search(k);
			break;
		}
		case 7:
		{
			int k;
			std::cin >> k;
			NOD<int>*c = new NOD<int>(k);
			b.DELETE(c);
			break;
		}
		case 8:
		{
			b.Print_Tree();

			break;
		}
		case 9:
		{
			std::cin >> n;
			v = new int[n];
			for (int i = 0; i < n; i++)
				std::cin >> v[i];
			b.construct(v, n);
			break;
		}
		case 10:
		{
			b.Emptry();

			break;
		}

		default:
			break;
		}
	}

	system("pause");
}